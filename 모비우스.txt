모비우스
oneM2M IoT 서버 플랫폼
oneM2M 은 IoT 기술을 위한 요구사항, 아키텍처, API 사양, 보안 솔루션, 상호 운용성을 제공하는 글로벌 단체
모비우스 플랫폼은 oneM2M 국제표준을 기반으로 사물인터넷 서비스 제공을 위해 다양한 IoT 디바이스 정보를 관리하고, 이들의 접근제어, 인증,
사용자 관리, 복수의 IoT 서비스 조합을 제공하여 어플리케이션을 통해 서비스하기 위한 플랫폼이다.
이러한 모비우스 서버 플랫폼은 디바이스와 어플리케이션을 연결하는 중간 매개체로써 디바이스는 서버 플랫폼으로 데이터를 전송하고, 
플랫폼은 데이터를 저장한다. 어플리케이션은 Open API 를 통해 플랫폼에 저장된 데이터를 조회하고 플랫폼으로 제어 요청을 전송한다.
플랫폼은 설정된 디바이스에 따라 어플리케이션의 제어요청을 다시 디바이스로 전달한다.
모비우스는 사물인터넷 장치와 어플리케이션을 연결하기 위해 http, CoAP, Mqtt WebSocket 프로토콜을 지원하고, 리소스는 MySQL DB에 저장한다.